package metadata

import (
	"fmt"
	"os"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

const (
	// AnalyzerVendor is the vendor/maintainer of the analyzer
	AnalyzerVendor = "GitLab"

	// AnalyzerName is the name of the analyzer
	AnalyzerName = "security-code-scan"

	scannerVendor = AnalyzerVendor
	scannerURL    = "https://security-code-scan.github.io"

	// scannerID identifies the scanner that generated the report
	scannerID = "security_code_scan"

	// scannerName identifies the scanner that generated the report
	scannerName = "Security Code Scan"

	// Type returns the type of the scan
	Type report.Category = report.CategorySast
)

var (
	// AnalyzerVersion is a placeholder value which the Dockerfile will dynamically
	// overwrite at build time with the most recent version from the CHANGELOG.md file
	AnalyzerVersion = "not-configured"

	// ScannerVersion is the semantic version of the scanner
	ScannerVersion = os.Getenv("SCANNER_VERSION")

	// IssueScanner describes the scanner used to find a vulnerability
	IssueScanner = report.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	// ReportScanner returns identifying information about a security scanner
	ReportScanner = report.ScannerDetails{
		ID:      scannerID,
		Name:    scannerName,
		Version: ScannerVersion,
		Vendor: report.Vendor{
			Name: scannerVendor,
		},
		URL: scannerURL,
	}

	// AnalyzerUsage provides a one line usage string for the analyzer
	AnalyzerUsage = fmt.Sprintf("%s %s analyzer v%s", AnalyzerVendor, AnalyzerName, AnalyzerVersion)
)
